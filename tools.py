from osgeo import gdal
import json
import rasterio as ro
import geopandas ad gpd
import pickle

"""
Get the bounding Box of a GDAL raster
"""
def get_raster_bbox(dataset):
    geotransform = dataset.GetGeoTransform()
    x_min = geotransform[0]
    y_max = geotransform[3]
    x_max = x_min + (geotransform[1] * dataset.RasterXSize)
    y_min = y_max + (geotransform[5] * dataset.RasterYSize)
    return x_min, y_min, x_max, y_max

"""
Get the index in a GDAL raster at a specific coordinate
"""
def get_index_at(dataset, coord):
    geotransform = dataset.GetGeoTransform()
    x,y = coord
    x_i = int(x / geotransform[1])
    y_i = abs(int(y / geotransform[5]))

    return (x_i, y_i)

"""
Read pixel to a numpy-Array within a specified extent (upperLeft, lowerRight) from a file
"""
def read_raster_within(raster, extent):

    r_read = gdal.Open(copDEM_path)
    band = r_read.GetRasterBand(1)
    
    xmin, ymax = extent[0]
    xmax, ymin = extent[1]

    xoff, yoff = get_raster_idx(r_read, (xmin, ymax))
    xoff2, yoff2 = get_raster_idx(r_read, (xmax, ymin))
    xspan = xoff2 - xoff
    yspan = yoff - yoff2 
    print(xspan, yspan)

    return band.ReadAsArray(xoff, yoff, xspan, yspan)

"""
Transform a bounding box into a geojson
"""
def bbox_to_geojson(upper_left, lower_right, name=""):
    geojson_data = {
        "type": "Feature",
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [upper_left[0], upper_left[1]],
                    [lower_right[0], upper_left[1]],
                    [lower_right[0], lower_right[1]],
                    [upper_left[0], lower_right[1]],
                    [upper_left[0], upper_left[1]],
                ]
            ]
        },
        "properties": {
            "name": name
        }
    }
    return json.dumps(geojson_data, indent=2)

"""
Write an numpy array to an GeoTiff, using a reference tiff.
"""
def arr2drive(arr, ref_path, export_dir = None):
    
    with ro.open(ref_path, 'r') as src_ref:
        transform = src_ref.transform
        crs = src_ref.crs

    height, width = arr.shape

    if export_dir == None:   
        export_dir = "/".join(ref_path.split("/")[:-1]) + "/export.tif"

    with ro.open(export_dir, 'w', driver='GTiff',
                 height=height, width=width, count=1,
                 dtype=arr.dtype, crs=crs, transform=transform) as dst:
        dst.write(arr, 1)

    return export_dir

"""
Exports an Earth Engine Image as Numpy-Array, defined by a scale (resoltion / pixel size) and within 
a defined region of interest ( roi)
"""
def get_eeImg_as_npArr(img, roi, scale):
    img_bounded = img.clipToBoundsAndScale(**{"geometry" : roi, "scale" : scale})
    request = {
    'expression' : img_bounded,
    'fileFormat': 'NUMPY_NDARRAY'
    }
    return ee.data.computePixels(request)

"""
rasterizes an vector file (vector_path) to an raster file based on a reference raster (ref_path)
"""
def rasterize(vector_path, ref_path, **kwargs):

    export_dir = kwargs.get("export_dir", "/".join(vector_path.split("/")[:-1]) + "/rasterized.tif")

    vector = gpd.read_file(vector_path)
 
    vector = vector.loc[vector.geometry.geom_type == "LineString"] # Filter for Line Strings
    geom = [shapes for shapes in vector.geometry]
  
    raster = ro.open(ref_path) 

    rasterized = features.rasterize(geom,
                                    out_shape= raster.shape,
                                    fill = 0,
                                    out = None,
                                    transform = raster.transform,
                                    all_touched = False,
                                    default_value=1,
                                    dtype = None)

    with ro.open(
        export_dir, "w",
        driver = "GTiff",
        crs = raster.crs,
        transform = raster.transform,
        dtype = ro.uint8,
        count = 1,
        width = raster.width,
        height = raster.height) as dst:
        dst.write(rasterized, indexes = 1)

    return export_dir

"""
concatenates a series of pd.Dataframes on disk (when not fitting into memory)
"""

def disk_concat(dataframes, **kwargs): 

    remove = kwargs.get("rem", True)
    fromdisk = kwargs.get("fromdisk", False)

    if fromdisk:
        file_path, df_lengths = dataframes
    else:
        df_lengths = []
        with open(data('dataframes_concat.pkl'), 'ab') as file: # initialize binary pickle file to write in append mode to
            for df in dataframes:
                pickle.dump(df, file) # append each dataframe to pickle file 
                df_lengths.append(len(df)) # save length of dataframe to later construct concatenated dataframe
        file_path = data('dataframes_concat.pkl')

    df_combined = pd.DataFrame()
    print(type(df_combined))
    with open(file_path, 'rb') as file:
        for l in range(0, len(df_lengths)):
            df_deserial = pickle.load(file)
            if df_combined.empty:
                df_combined = df_deserial
            else:
                df_combined = pd.concat([df_combined, df_deserial])
    
    if remove:
        os.remove(file_path) # removes binary from disk

"""
Get OSM Features within a defined bounding box, by a defined osmnx query.
"""
def get_osm_features(bounds, query, export_dir):
    x1, y1, x2, y2 = bounds 
    try:
        osm_features = ox.features_from_bbox(y2, y1, x2, x1, query)
        osm_features["geometry"].to_file(export_dir, driver = 'GPKG')
        print(f"Fetched OSM Data for: : N{y1} E{x1} to N{y2} E{x2}")
        return export_dir
    except:
        print(f"Could not fetch OSM Data for: N{y1} E{x1} to N{y2} E{x2}")
        return None

"""
Calculate a distance raster from an ee.Image() (needs to be a mask with binary (0 or 1) values) in Earth Engine.
Required:
    - img: The binary ee.Image()
    - bounds: The bounds in which to calcuate the distances ((x1, y1), (x2, y2), (x3, y3), (x4, y4))
"""
def calc_ee_dist_rast(img, bounds):

    # Calculate the Extent (xmin, ymin, xmax, ymax) from the provided bounding points
    x_sorted = lambda index : sorted(bounds, key=lambda x: x[0])[index][0]
    y_sorted = lambda index : sorted(bounds, key=lambda x: x[1])[index][1]
    
    extent = [x_sorted(0), y_sorted(0), x_sorted(-1), y_sorted(-1)]
    extent = ee.Geometry.Rectangle(extent) # convert Extent to GG Geometry
    
    # Calculate the diagonal from the Extent -> highest possible search-radius
    extent_span = round(ee.Geometry.Point(x_sorted(0), y_sorted(0))\
                        .distance(ee.Geometry.Point(x_sorted(-1), y_sorted(-1))).getInfo(),0)
    
    # Vectorize the provided ImgMask (img) to perform ee.Feature.distance() function
    vectorized = img.reduceToVectors(**{"geometry": extent, "scale":30,"crs":'EPSG: 4326',"maxPixels":1e13})
    dist_raster = vectorized.distance(**{"searchRadius" : extent_span}).clip(extent).rename("distance")

    return dist_raster


"""
Simple EE helper to get the minimum and maximum of an ee.Image using geemap.
"""
def minmax(img):
    minmaxval = lambda img, band, minmax: round(gp.image_stats(img).getInfo()[minmax][band], 2)
    maxval = minmaxval(img, "distance", "max")
    minval = minmaxval(img, "distance", "min")
    return minval, maxval