# Creates a cursor map

cursor = r -> [[(x, y) for x in (r *-1):r] for y in (r *-1):r]
